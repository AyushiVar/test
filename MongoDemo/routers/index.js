const express = require('express');
const router = express.Router();
//const mongo = require('mongodb').MongoClient;
//const assert = require('assert');
//var url = 'mongodb://localhost:27017/Mongo-data';
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/Mongo-data', {useNewUrlParser: true } );
var Schema = mongoose.Schema;
var crypto = require('crypto');
const nodemailer = require('nodemailer'); 

let transportar = nodemailer.createTransport({
    service : 'gmail',
    auth:{
        user: 'ayushi.intellirise21@gmail.com',
        pass: '******'
    }
});

var scheduleSchema = new Schema({
    Name: String,
    Reason: String,
    Email: String,
    ReferedBy: String,
    PhoneNumber: String,
    Message: String
});

var UserSchema = new Schema({
    FirstName: String,
    LastName: String,
    Email: String,
    hash: String,
    salt: String
});

UserSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

UserSchema.methods.validatePassword = function(password){
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

var Appoinment = mongoose.model('Appoinment', scheduleSchema);
var Registration = mongoose.model('Registration', UserSchema);

router.get('/', function(req,res){
    console.log("responding in browser");
    res.render('demo');
});

/* router.post('/schedule', function(req, res, next){
    var item = {
        Name: req.body.name,
        Reason: req.body.reason,
        Email: req.body.email,
        ReferedBy: req.body.refer,
        PhoneNumber: req.body.phone,
        Message: req.body.message
    };

    mongo.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection('Appoinment').insertOne(item, function(err, result){
            assert.equal(null, err);
            console.log("Appointment scheduled");
            res.send("Succesfuly scheduled");
            res.end();
        });
    });
}); */

router.post('/schedule', function(req, res, next){
    let item = {
        Name: req.body.name,
        Reason: req.body.reason,
        Email: req.body.email,
        ReferedBy: req.body.refer,
        PhoneNumber: req.body.phone,
        Message: req.body.message
    };
    let data = new Appoinment(item);
    data.save((err, data)=>{
        if(err){
            return res.status(400).send({ 
                message : "Failed to schedule appoinment"
            }); 
        }
        else{
            let mailOptions = {
                from: 'ayushi.intellirise21@gmail.com',
                to: 'mail.raviithakur@gmail.com',
                subject:'Sending mail using Node.js',
                text: 'Hello Ravi,\nYour appoinment has been scheduled with us'
            };
            transportar.sendMail(mailOptions, function(error, info){
                if(error){
                    return res.status(400).send({ 
                        message : error
                    });
                }
                else{
                    return res.status(201).send({
                        message: "Your aapoinment has been scheduled. "+
                                 "Please check your mail for more info"
                    });
                }
            });
            /* return res.status(201).send({
                message : "Sucessfully scheduled"
            }); */
        }
    });
});

router.post('/register', function(req, res, next){
    let newUser = new Registration();

    newUser.FirstName = req.body.fName;
    newUser.LastName = req.body.lName;
    newUser.Email = req.body.email;
    newUser.setPassword(req.body.password);

    newUser.save(function(err, user){
        if(err){
            return res.status(400).send({
                message : "Failed to add user"
            });
        }
        else{
            return res.status(201).send({
                message : "User Registered successfully"
            });
        }
    });
});

router.post('/login', function(req, res, next){
    Registration.findOne({Email:(req.body.email).trim()}, function(err, user){
        if(user === null){
            return res.status(400).send({
                message : "No data found for entred email"
            });
        }
        else{
            if(user.validatePassword(req.body.password)){
                Registration.findOne({
                     Email: (req.body.email).trim()
                }).then((doc) => {    
                    //console.log(JSON.stringify(doc));
                    res.cookie("loginData", doc);
                    res.send(req.cookies);
                }).catch(err => {    
                    console.log(err);  
                });
                /* res.status(201).send({
                    message : "User logged in"
                }); */
            }
            else{
                res.status(400).send({
                    message : "Wrong Password"
                });
            }
        }
    }); 
});

router.get('/logout', (req, res)=>{ 
    //it will clear the userData cookie 
    res.clearCookie('loginData'); 
    res.send('user logout successfully'); 
    }); 

module.exports = router;