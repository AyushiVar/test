const http = require('http');
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const hbs = require('express-handlebars');

const routes = require('./routers/index.js');

const app = express();

app.engine('hbs', hbs({extname: 'hbs', defaultLayout: 'layout', layoutsDir: path.join('localhost:8080','\mong\views\layouts')}));
//app.set('views', path.join(__dirname, 'views'));
//app.set('views', path.join('localhost:8080/mong/views'));
app.set('view engine', 'hbs');

//app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.use('/',routes);

app.use(function(req, res, next){
    var err= new Error('Not Found');
    err.status = 404;
    next(err);   
});

http.createServer().listen(3003,function(req, res){
    console.log("The server is up...");
});