const express = require('express');
const app = express();
const connection = require('./db');
const transportar = require('./mailerAuth');
const path = require('path');
// morgan = require('morgan');
const bodyParser = require('body-parser');
const multer = require('multer');
const sessions = require('express-session');
const hbs = require('express-handlebars');
const crypto = require('crypto');
var PDFDocument = require('pdfkit');
const fs = require('fs');
const generator = require('generate-password');

var session;

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/');
    },
    filename: function(req, file, cb){
        cb(null, file.originalname);
    }
});

const upload = multer({storage: storage});

app.engine('hbs', hbs({extname: 'hbs', defaultLayout: 'layout', layoutsDir: __dirname + '/views/layouts/'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

//app.use(morgan ('short'));
app.use(bodyParser.json({limit:'50mb'})); 
app.use(bodyParser.urlencoded({extended:true, limit:'50mb'}));
app.use(express.static('./views'));

app.use('/uploads', express.static('uploads'));

app.use(sessions({
    secret: '%$#%$65468sdvhv657^&%ryfJS',
    saveUninitialized: true,
    resave: false,
   
}));

app.get('/', function(req, res, next){
    console.log("Responding in Browser..");
    res.sendFile('index.html');
});

app.get('/createPDF', (req, res)=>{
    const doc = new PDFDocument;
    doc.pipe(fs.createWriteStream(__dirname+'/output.pdf'));
    doc.fontSize(25)
       .text('Some text with an embedded font!', 100, 100);
    doc.image(__dirname+'/uploads/flowers.jpg', {
       fit: [250, 300],
       align: 'center',
       valign: 'center'
    });
    doc.end();
    console.log("created");
    res.end("Created");
});

app.get('/gt-file', function (req, res) {
    var filePath = __dirname+"/output.pdf";

    fs.readFile(filePath , function (err,data){
        res.contentType("application/pdf");
        res.send(data);
    });
});

app.get('/downloadFile', function (req, res) {
    var file = __dirname +'/output.pdf';
    res.download(file, function (err) {
        if (err) {
            console.log("Error");
            console.log(err);
        } else {
            console.log("Success");
        }
    });
 });

//this one is working
app.post('/message', (req, res) => {
    console.log(".......");
    const name = req.body.name;
    const email = req.body.email;
    const phone = req.body.phone;
    const message = req.body.message;
    const deleted = 1;

    const querystring = "INSERT INTO Messages (name, email, phone, message, created_at, updated_at, deleted_at)"
                        + "VALUES(?,?,?,?,now(),now(),?)"; 

    connection.query(querystring, [name, email, phone, message, deleted], (err, results, fields)=>{
        if(err){
            return res.status(400).send({ 
                message : error
            });
        }else{
            let mailOptions = {
                from: 'ayushi.intellirise21@gmail.com',
                to: 'ayushivarshney41@gmail.com ',
                subject:'Testing in MySql',
                text: 'Your message has been sent'
            };
            transportar.sendMail(mailOptions, function(error, info){
                if(error){
                    return res.status(400).send({ 
                        message : error
                    });
                }
                else{
                    return res.status(201).send({
                        message: "Your message has been sent "+
                                 "Please check your mail for more info"
                    });
                }
            });
        }
    });
});

app.post('/register', (req, res) =>{
    const fName = req.body.fName;
    const lName = req.body.lName;
    const email = req.body.email;
    const password = (req.body.password).trim();
    const deleted = 1;
    if(email !== undefined){
        const querystring = "INSERT INTO registrationdetails (firstname, lastname, email, password, transactionValue," 
                            + "created_at, updated_at, deleted_at) VALUES(?,?,?,?,0,now(),now(),?)";

        connection.query(querystring, [fName, lName, email, password, deleted], (err, rows,fields) => {
            if(err){
                return res.status(400).send({ 
                    message : err
                });
            }
            else{
                let mailOptions = {
                    from: 'ayushi.intellirise21@gmail.com',
                    to: email,
                    subject:'Sending mail using Node.js',
                    text: 'Thank youfor registering with us.'
                };
                transportar.sendMail(mailOptions, function(error, info){
                    if(error){
                        connection.rollback(function(err){
                            console.log(error);
                            return res.status(400).send({ 
                                message : "Invalid Email"
                            });
                        }); 
                    }
                    else{
                        console.log("here");
                        console.log(rows);
                        res.render('signIn');
                    }
                });
            }
        });
    }
});

app.post('/schedule', (req, res) =>{
    const name = req.body.name;
    const reason = req.body.reason;
    const email= req.body.email;
    const refer = req.body.refer;
    const phone = req.body.phone;
    const message = req.body.message;

    const querystring ="INSERT INTO Appoinments (name, reason, emailid, refferedby, phone, message, created_at," +
                        "updated_at, deleted_at) VALUES(?,?,?,?,?,?,now(),now(),1)";
    connection.query(querystring, [name, reason, email, refer, phone, message], (err, results, fields)=>{
        if(err){
            return res.status(400).send({ 
                message : error
            });
        }else{
        let mailOptions = {
            from: 'ayushi.intellirise21@gmail.com',
            to: 'ayushivarshney41@gmail.com',
            subject:'Sending mail using Node.js',
            text: 'Thank youfor registering with us'
        };
        transportar.sendMail(mailOptions, function(error, info){
            if(error){
                connection.rollback(function(err){
                    console.log(error);
                    return res.status(400).send({ 
                        message : "Invalid Email"
                    });
                }); 
            }
            else{
                res.sendFile('index.html');
            }
        });
    }
    });
});

 app.post('/sendReport',upload.single('report'), (req, res)=>{
    const pat_name = req.body.name;
    const mobile = req.body.mobile;
    const email = req.body.email;
    const doc_fname = req.body.fname;
    const doc_lanme= req.body.lname;
    const clinicName = req.body.clinic;
    const hosName = req.body.hospital;
    const city = req.body.city;
    const state = req.body.state;
    const report = req.file.path;
    
    const querystring = "INSERT INTO patientreport (patientname, mobile, email, doctorfirstname, doctorlastname,"
                        +"clinicname, hospitalname, city, state, upload_report, created_at, updated_at, deleted_at)"
                        +" VALUES(?,?,?,?,?,?,?,?,?,?,now(),now(),1)";
    
    connection.query(querystring, [pat_name,mobile,email,doc_fname,doc_lanme,clinicName,hosName
                        ,city,state,report], (err, rows, fields)=>{
        if(err){
            return res.status(400).send({ 
                message : error
            });
        }
        res.send(Success);
    });
}); 

app.all('/login',(req, res)=>{
    const email = req.body.email;
    const password = req.body.password;
    const querystring = "SELECT * from registrationdetails where email = ? and password = ?";
    connection.query(querystring, [email, password], (err, rows, fields)=>{
        if(err){
            throw err;
        }
        session = req.session;
        if(rows.length > 0){
            /* if(session.uniqueID){
                res.redirect('/redirects');
            }  */
            if((email).trim() == rows[0].Email && password == rows[0].Password){
                session.uniqueID = "truGene_" + rows[0].FirstName + rows[0].Id;
                session.transaction = rows[0].transactionValue;
                session.firstName = rows[0].FirstName;
                session.lastName = rows[0].LastName;
                console.log(session);
                //console.log(JSON.stringify(rows));
                if(rows[0].transactionValue == 0)
                    res.render('login', {patientName: rows[0].FirstName, lastName: rows[0].LastName});
                else
                    res.render('your-reports', {patientName: rows[0].FirstName, lastName: rows[0].LastName}); 
            }
            else{
                res.send("Invlid User/ Password");
            } 
        }
    });
}); 

app.get('/logout', function(req, res){
    req.session.destroy(function(err){
        if(err){
            return res.status(404).send(err);
        }
        else{
            console.log("jghjkgakdbck");
            res.render('signIn');
        }
    })
});

app.get('/redirects',  function(req, res){
    session = req.session;
    if(session.uniqueID){
        res.render('login.hbs');
    }
    else{
        res.end("Session TImeout! To open new session click <a href = '/logout'>Kill Session</a>");
    }
}); 

app.all('/forgotPassword', function(req,res){
    console.log("1");
    const email = req.body.email;
    console.log("2");
    const password = generator.generate({
        length: 10,
        numbers: true
    });
    console.log("3");
    console.log("4");
    let querystring = "SELECT * FROM registrationdetails WHERE Email = ?";
    console.log(email);
    console.log(password);
    connection.query(querystring, [email], (err, rows, field)=>{
        console.log(rows);
        if(err){
            return res.status(400).send({
                message : err
            });
        }
        else if(rows.length == 0){
            return res.status(404).send("Email address not registered with us");
        }
        else{
            querystring = "UPDATE registrationdetails SET Password = ? where Id = ?";
            connection.query(querystring, [password, rows[0].Id], (err, results, fields) =>{
                if(err){
                    connection.rollback(function(){
                        return res.status(400).send({ 
                            message : err
                        });
                    });
                }
                else{
                    let mailOptions = {
                        from: 'ayushi.intellirise21@gmail.com',
                        to: email,
                        subject:'Sending mail using Node.js',
                        text: 'Your password is '+password
                    };
                    transportar.sendMail(mailOptions, function(error, info){
                        if(error){
                            connection.rollback(function(){
                                console.log(error);
                                return res.status(400).send({ 
                                    message : "Invalid email"
                                });
                            }); 
                        }
                        else{
                            res.render('signIn');
                        }
                    });
                }
            });
        }
    });
    
    });

app.post('/healthHistory', function(req, res){
    const DOB = req.body.dob;
    const Height = req.body.Height
    const Weight = req.body.Weight;
    const BloodPressure = req.body.BloodPressure;
    const Ciggerate = req.body.Ciggerate;
    const Alcohol = req.body.Alcohol;
    const Diabetes = req.body.Diabetes;
    const MedicationBP= req.body.MedicationBP;
    const Prescriptions = req.file.path;
    const Medication = req.body.Medication;
    const PrescriptionMedication = req.body.PrescriptionMedication;
    const LiverTransplant = req.body.LiverTransplant;
    const SeeingDoctor = req.body.SeeingDoctor;
    const KidneyDetails = req.KidneyDetails;
    const Cancer = req.body.Cancer;
    const CancerDetails = req.body.CancerDetails;
    const ColonPolyps = req.body.ColonPolyps;
    //const ColonPolyps = req.body.ColonPolyps;
    const ColonPolypsNumbers = req.body.ColonPolypsNumbers;
    const MarrowTransplant = req.body.MarrowTransplant;
    const TransplantedBone = req.body.TransplantedBone;
    const TransfusionWeek = req.body.TransfusionWeek;
    const GeneticTest = req.body.GeneticTest;
    const WhoGenetic = req.body.WhoGenetic;
    const Mutation = req.body.Mutation;
    const MutationGene = req.body.MutationGene;
    const Aneurysm = req.body.Aneurysm;
    const AneurysmAge = req.body.AneurysmAge;
    const AneurysmLocation = req.body.AneurysmLocation;
    const Dissection = req.body.Dissection;
    const DissectionAge = req.body.DissectionAge;
    const DissectionLocation = req.body.DissectionLocation;
    const Fainting = req.body.Fainting;
    const Arrythmia = req.body.Arrythmia;
    const ArrythmiaAge = req.body.ArrythmiaAge;
    const ArrythmiaType = req.body.ArrythmiaType;
    const Cardiomegaly = req.body.Cardiomegaly;
    const CardiomegalyAge = req.body.CardiomegalyAge;
    const HeartAttack = req.body.HeartAttack;
    const HeartAttackAge = req.body.HeartAttackAge;
    const CardiacArrest = req.body.CardiacArrest;
    const CardiacArrestAge = req.body.CardiacArrestAge;
    const Stroke = req.body.Stroke;
    const StrokeAge = req.body.StrokeAge;
    const Cholestrol = req.body.Cholestrol;
    const CholestrolDeposit = req.body.CholestrolDeposit;
    const DepositDetails = req.body.DepositDetails;
    const CornealArcus = req.body.CornealArcus;
    const CornealArcusAge = req.body.CornealArcusAge;
    const GeneticHeredity = req.body.GeneticHeredity;
    const GeneticHeredityWho = req.body.GeneticHeredityWho;
    const GeneticHeredityMutation = req.body.GeneticHeredityMutation;
    

    const querystring = "INSERT INTO HEALTHHISTORY (UserId, DOB, Height, Weight, BloodPressure, Ciggerate, Alcohol,"
                        + "Diabetes, MedicationBP, Prescriptions, Medication, PrescriptionMedication, LiverTransplant,"
                        + "SeeingDoctor, KidneyDetails, Cancer, CancerDetails, ColonPolyps, ColonPolypsNumbers,"
                        + "MarrowTransplant, TransplantedBone, TransfusionWeek, GeneticTest, WhoGenetic, Mutation,"
                        + "MutationGene, Aneurysm, AneurysmAge, AneurysmLocation, Dissection, DissectionAge, DissectionLocation,"
                        + "Fainting, Arrythmia, ArrythmiaAge, ArrythmiaType, Cardiomegaly, CardiomegalyAge, HeartAttack,"
                        + "HeartAttackAge, CardiacArrest, CardiacArrestAge, Stroke, StrokeAge, Cholestrol, CholestrolDeposit,"
                        + "DepositDetails, CornealArcus, CornealArcusAge, GeneticHeredity, GeneticHeredityWho,)"
                        + "GeneticHeredityMutation VLAUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";    
    connection.query(querystring, [session.uniqueID, DOB, Height, Weight, BloodPressure, Ciggerate, Alcohol, Diabetes,
                                        MedicationBP,Prescriptions, Medication, PrescriptionMedication,LiverTransplant, 
                                        SeeingDoctor, KidneyDetails, Cancer, CancerDetails, ColonPolyps,ColonPolypsNumbers,
                                        MarrowTransplant, TransplantedBone, TransfusionWeek, GeneticTest, WhoGenetic, Mutation,
                                        MutationGene, Aneurysm, AneurysmAge, AneurysmLocation, Dissection,DissectionAge,
                                        DissectionLocation,Fainting, Arrythmia, ArrythmiaAge, ArrythmiaType, Cardiomegaly,
                                        CardiomegalyAge, HeartAttack,HeartAttackAge, CardiacArrest, CardiacArrestAge, Stroke,
                                        StrokeAge, Cholestrol, CholestrolDeposit,DepositDetails, CornealArcus, CornealArcusAge,
                                        GeneticHeredity, GeneticHeredityWho, GeneticHeredityMutation], function(err,rows,fields){
        if(err){
            throw err;
        }
        res.send("Saved");
    });
});

app.all('/signIn', (req, res)=>{
    res.render('signIn');
});
app.all('/your-report',function(req,res,next){
    console.log("Inside your report");
    session = req.session;
    console.log(session);
    if(session.transaction==0){
        res.render('login', {patientName: session.firstName, lastName:session.lastName});
    }
    else{
        res.render('your-reports',{patientName: session.firstName, lastName:session.lastName});
    }
});
app.all('/activation-kit',function(req,res,next){
    session = req.session;
    res.render('Activate-kit',{patientName: session.firstName, lastName: session.lastName});
});

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send('error', {
            message: err.message, 
            error: err
        });
     });
 }

app.listen(3003,function(req, res){
    console.log("The server is up...");
});
