-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2019 at 10:03 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trugene`
--

-- --------------------------------------------------------

--
-- Table structure for table `appoinments`
--

CREATE TABLE `appoinments` (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Reason` varchar(500) DEFAULT NULL,
  `EmailId` varchar(100) DEFAULT NULL,
  `RefferedBy` varchar(100) DEFAULT NULL,
  `Phone` varchar(15) DEFAULT NULL,
  `Message` varchar(1000) DEFAULT NULL,
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Deleted_at` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appoinments`
--

INSERT INTO `appoinments` (`Id`, `Name`, `Reason`, `EmailId`, `RefferedBy`, `Phone`, `Message`, `Created_at`, `Updated_at`, `Deleted_at`) VALUES
(1, 'Joe', 'Test', 'Joe@org', 'Ms. Clarke', '2147483647', 'Testing', '2019-05-16 05:26:40', '2019-05-16 05:26:40', 1),
(2, 'Ayushi', 'Test1', 'abc@gmail.com', 'Ravi Kumar', '2147483647', 'Integration Test', '2019-05-19 20:45:16', '2019-05-19 20:45:16', 1),
(3, 'Ayushi', 'Test2', 'abc@gmail.com', 'Ravi Kumar', '8888888888', 'Integration Test for phone', '2019-05-19 20:49:14', '2019-05-19 20:49:14', 1),
(4, 'John', 'Testing', 'John.Do@gmail.com', 'Ravi Kumar', '8888888888', 'Testing successful', '2019-06-08 19:50:56', '2019-06-08 19:50:56', 1),
(5, 'John', 'Testing', 'John.Do@gmail.com', 'Ravi Kumar', '8888888888', 'Testing successful', '2019-06-08 19:53:48', '2019-06-08 19:53:48', 1),
(6, 'John', 'Testing', 'John.Do@gmail.com', 'Ravi Kumar', '8888888888', 'Testing successful', '2019-06-08 19:57:06', '2019-06-08 19:57:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `geneservices`
--

CREATE TABLE `geneservices` (
  `Id` int(11) NOT NULL,
  `ServiceId` int(11) DEFAULT NULL,
  `ServiceName` varchar(100) DEFAULT NULL,
  `ServiceOrder` varchar(255) NOT NULL,
  `ServiceDescription` varchar(500) DEFAULT NULL,
  `ServicePrice` double DEFAULT NULL,
  `CreatedAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DeletedAt` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `geneservices`
--

INSERT INTO `geneservices` (`Id`, `ServiceId`, `ServiceName`, `ServiceOrder`, `ServiceDescription`, `ServicePrice`, `CreatedAt`, `UpdatedAt`, `DeletedAt`) VALUES
(1, 1, 'Free', '1Order your truGene test.', 'Get complimentary 30 minutes of Pre-test and 30 minutes of Post-test genetic counselling', 0, NULL, NULL, NULL),
(2, 2, 'Preliminary Session', '2Order your truGene test.', 'Get complimentary 30 minutes of Pre-test and 30 minutes of Post-test genetic counselling', 550, NULL, NULL, NULL),
(3, 3, 'Comprehensive Session', '3Order your truGene test.', 'Get complimentary 30 minutes of Pre-test and 30 minutes of Post-test genetic counselling', 750, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `healthhistory`
--

CREATE TABLE `healthhistory` (
  `Id` int(11) NOT NULL,
  `UserId` varchar(100) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Height` varchar(20) DEFAULT NULL,
  `Weight` varchar(20) DEFAULT NULL,
  `BloodPressure` varchar(50) DEFAULT NULL,
  `Ciggerate` binary(1) DEFAULT NULL,
  `Alcohol` tinyint(4) DEFAULT NULL,
  `Diabetes` binary(1) DEFAULT NULL,
  `MedicationBP` binary(1) DEFAULT NULL,
  `Prescriptions` varchar(500) DEFAULT NULL,
  `Medication` binary(1) DEFAULT NULL,
  `PrescriptionMedication` varchar(500) DEFAULT NULL,
  `LiverTransplant` binary(1) DEFAULT NULL,
  `SeeingDoctor` binary(1) DEFAULT NULL,
  `KidneyDetails` varchar(500) DEFAULT NULL,
  `Cancer` binary(1) DEFAULT NULL,
  `CancerDetails` varchar(200) DEFAULT NULL,
  `ColonPolyps` tinyint(4) DEFAULT NULL,
  `ColonPolypsNumbers` tinyint(4) DEFAULT NULL,
  `MarrowTransplant` varchar(50) DEFAULT NULL,
  `TransplantedBone` varchar(50) DEFAULT NULL,
  `TransfusionWeek` binary(1) DEFAULT NULL,
  `GeneticTest` binary(1) DEFAULT NULL,
  `WhoGenetic` varchar(100) DEFAULT NULL,
  `Mutation` tinyint(4) DEFAULT NULL,
  `MutationGene` varchar(100) DEFAULT NULL,
  `Aneurysm` binary(1) DEFAULT NULL,
  `AneurysmAge` tinyint(4) DEFAULT NULL,
  `AneurysmLocation` varchar(50) DEFAULT NULL,
  `Dissection` binary(1) DEFAULT NULL,
  `DissectionAge` tinyint(4) DEFAULT NULL,
  `DissectionLocation` varchar(50) DEFAULT NULL,
  `Fainting` binary(1) DEFAULT NULL,
  `Arrythmia` binary(1) DEFAULT NULL,
  `ArrythmiaAge` tinyint(4) DEFAULT NULL,
  `ArrythmiaType` varchar(50) DEFAULT NULL,
  `Cardiomegaly` binary(1) DEFAULT NULL,
  `CardiomegalyAge` tinyint(4) DEFAULT NULL,
  `HeartAttack` binary(1) DEFAULT NULL,
  `HeartAttackAge` tinyint(4) DEFAULT NULL,
  `CardiacArrest` binary(1) DEFAULT NULL,
  `CardiacArrestAge` tinyint(4) DEFAULT NULL,
  `Stroke` binary(1) DEFAULT NULL,
  `StrokeAge` tinyint(4) DEFAULT NULL,
  `Cholestrol` tinyint(4) DEFAULT NULL,
  `CholestrolDeposit` binary(1) DEFAULT NULL,
  `DepositDetails` varchar(50) DEFAULT NULL,
  `CornealArcus` binary(1) DEFAULT NULL,
  `CornalArcusAge` tinyint(4) DEFAULT NULL,
  `GeneticHeredity` binary(1) DEFAULT NULL,
  `GeneticHeredityWho` varchar(100) DEFAULT NULL,
  `GeneticHeredityMutation` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `health_history`
--

CREATE TABLE `health_history` (
  `Id` int(11) NOT NULL,
  `UserId` varchar(100) DEFAULT NULL,
  `Month` varchar(20) DEFAULT NULL,
  `Day` varchar(20) DEFAULT NULL,
  `Year` varchar(20) DEFAULT NULL,
  `Weight` varchar(20) DEFAULT NULL,
  `BloodPressure1` varchar(10) DEFAULT NULL,
  `BloodPressure2` varchar(10) DEFAULT NULL,
  `Ciggerate` tinyint(1) DEFAULT NULL,
  `Alcohol` tinyint(1) DEFAULT NULL,
  `Diabetes` tinyint(1) DEFAULT NULL,
  `MedicationBP` tinyint(1) DEFAULT NULL,
  `Prescriptions` varchar(500) DEFAULT NULL,
  `Medication` tinyint(1) DEFAULT NULL,
  `PrescriptionMedication` varchar(500) DEFAULT NULL,
  `LiverTransplant` tinyint(1) DEFAULT NULL,
  `SeeingDoctor` tinyint(1) DEFAULT NULL,
  `KidneyDetails` varchar(500) DEFAULT NULL,
  `Cancer` tinyint(1) DEFAULT NULL,
  `CancerDetails` varchar(500) DEFAULT NULL,
  `OtherCancer` varchar(100) DEFAULT NULL,
  `ColonPolyps` tinyint(1) DEFAULT NULL,
  `ColonPolypsNumbers` tinyint(4) DEFAULT NULL,
  `MarrowTransplant` varchar(50) DEFAULT NULL,
  `TransplantedBone` varchar(50) DEFAULT NULL,
  `TransfusionWeek` tinyint(1) DEFAULT NULL,
  `GeneticTest` tinyint(1) DEFAULT NULL,
  `WhoGenetic` varchar(100) DEFAULT NULL,
  `Mutation` tinyint(1) DEFAULT NULL,
  `MutationGene` varchar(100) DEFAULT NULL,
  `Aneurysm` tinyint(1) DEFAULT NULL,
  `AneurysmAge` tinyint(4) DEFAULT NULL,
  `AneurysmLocation` varchar(50) DEFAULT NULL,
  `Dissection` tinyint(1) DEFAULT NULL,
  `DissectionAge` tinyint(4) DEFAULT NULL,
  `DissectionLocation` varchar(50) DEFAULT NULL,
  `Fainting` tinyint(1) DEFAULT NULL,
  `Arrythmia` tinyint(1) DEFAULT NULL,
  `ArrythmiaAge` tinyint(4) DEFAULT NULL,
  `ArrythmiaType` varchar(500) DEFAULT NULL,
  `Cardiomegaly` tinyint(1) DEFAULT NULL,
  `CardiomegalyAge` tinyint(4) DEFAULT NULL,
  `HeartAttack` tinyint(1) DEFAULT NULL,
  `HeartAttackAge` tinyint(4) DEFAULT NULL,
  `CardiacArrest` tinyint(1) DEFAULT NULL,
  `CardiacArrestAge` tinyint(4) DEFAULT NULL,
  `Stroke` tinyint(1) DEFAULT NULL,
  `StrokeAge` tinyint(4) DEFAULT NULL,
  `Cholestrol` tinyint(1) DEFAULT NULL,
  `CholestrolDeposit` tinyint(1) DEFAULT NULL,
  `DepositDetails` varchar(100) DEFAULT NULL,
  `CornealArcus` tinyint(1) DEFAULT NULL,
  `CornalArcusAge` tinyint(4) DEFAULT NULL,
  `GeneticHeredity` tinyint(1) DEFAULT NULL,
  `GeneticHeredityWho` varchar(100) DEFAULT NULL,
  `GeneticHeredityMutation` tinyint(1) DEFAULT NULL,
  `Adopted` binary(1) DEFAULT NULL,
  `HaveChildren` binary(1) DEFAULT NULL,
  `NumbersOfDaughetrs` tinyint(4) DEFAULT NULL,
  `DaughterFName` varchar(50) DEFAULT NULL,
  `DaughterLName` varchar(50) DEFAULT NULL,
  `DaughterAge` int(11) DEFAULT NULL,
  `DaughterAlive` varchar(20) DEFAULT NULL,
  `NumbersOfSons` tinyint(4) DEFAULT NULL,
  `SonFName` varchar(50) DEFAULT NULL,
  `SonLName` varchar(50) DEFAULT NULL,
  `SonAge` int(11) DEFAULT NULL,
  `SonAlive` varchar(20) DEFAULT NULL,
  `Siblings` tinyint(4) DEFAULT NULL,
  `Sisters` tinyint(4) DEFAULT NULL,
  `SisterFName` varchar(50) DEFAULT NULL,
  `SisterLName` varchar(50) DEFAULT NULL,
  `SisterAge` int(11) DEFAULT NULL,
  `SisterAlive` varchar(10) DEFAULT NULL,
  `Brothers` tinyint(4) DEFAULT NULL,
  `BrotherFName` varchar(50) DEFAULT NULL,
  `BrotherLName` varchar(50) DEFAULT NULL,
  `BrotherAge` int(11) DEFAULT NULL,
  `BrotherAlive` varchar(10) DEFAULT NULL,
  `MotherFName` varchar(50) DEFAULT NULL,
  `MotherLName` varchar(50) DEFAULT NULL,
  `MotherAge` int(11) DEFAULT NULL,
  `MotherAlive` varchar(10) DEFAULT NULL,
  `FatherFName` varchar(50) DEFAULT NULL,
  `FatherLName` varchar(50) DEFAULT NULL,
  `FatherAge` int(11) DEFAULT NULL,
  `FatherAlive` varchar(10) DEFAULT NULL,
  `MaternalGrandMotherFName` varchar(50) DEFAULT NULL,
  `MaternalGrandMotherLName` varchar(50) DEFAULT NULL,
  `MaternalGrandMotherAge` int(11) DEFAULT NULL,
  `MaternalGrandMotherAlive` varchar(10) DEFAULT NULL,
  `MaternalGrandFatherFName` varchar(50) DEFAULT NULL,
  `MaternalGrandFatherLName` varchar(50) DEFAULT NULL,
  `MaternalGrandFatherAge` int(11) DEFAULT NULL,
  `MaternalGrandFatherAlive` varchar(10) DEFAULT NULL,
  `ParentalGrandMotherFName` varchar(50) DEFAULT NULL,
  `ParentalGrandMotherLName` varchar(50) DEFAULT NULL,
  `ParentalGrandMotherAge` int(11) DEFAULT NULL,
  `ParentalGrandMotherAlive` varchar(10) DEFAULT NULL,
  `ParentalGrandFatherFName` varchar(50) DEFAULT NULL,
  `ParentalGrandFatherLName` varchar(50) DEFAULT NULL,
  `ParentalGrandFatherAge` int(11) DEFAULT NULL,
  `ParentalGrandFatherAlive` varchar(10) DEFAULT NULL,
  `Country` varchar(50) DEFAULT NULL,
  `MotherSibling` tinyint(1) DEFAULT NULL,
  `NMS` int(11) DEFAULT NULL,
  `NMB` int(11) DEFAULT NULL,
  `FatherSibling` tinyint(1) DEFAULT NULL,
  `NFS` int(11) DEFAULT NULL,
  `NFB` int(11) DEFAULT NULL,
  `FamilyCancer` varchar(100) DEFAULT NULL,
  `FBrothersNumberCancer` int(11) DEFAULT NULL,
  `FCancerNames` varchar(500) DEFAULT NULL,
  `FGeneticForCancerRisk` tinyint(1) DEFAULT NULL,
  `FH` varchar(100) DEFAULT NULL,
  `FMutationIdentified` tinyint(1) DEFAULT NULL,
  `FMutationGene` varchar(100) DEFAULT NULL,
  `FamilyMember` varchar(100) DEFAULT NULL,
  `desc1` tinyint(1) DEFAULT NULL,
  `value1` varchar(500) DEFAULT NULL,
  `FCardiomyopathy` tinyint(1) DEFAULT NULL,
  `FCardiomyopathyDesc` varchar(500) DEFAULT NULL,
  `FHeredityArrhthmia` tinyint(1) DEFAULT NULL,
  `FArteriopathy` tinyint(1) DEFAULT NULL,
  `FHeartFailure` tinyint(1) DEFAULT NULL,
  `FHeartFailureDesc` varchar(500) DEFAULT NULL,
  `FHeartFailureAge` int(11) DEFAULT NULL,
  `FEnlargedHeart` tinyint(1) DEFAULT NULL,
  `FEnlargedHeartDesc` varchar(500) DEFAULT NULL,
  `FHeartAttack` tinyint(1) DEFAULT NULL,
  `FStroke` tinyint(1) DEFAULT NULL,
  `FStrokeDesc` varchar(500) DEFAULT NULL,
  `FStrokeAge` int(11) DEFAULT NULL,
  `FAneurysm` tinyint(1) DEFAULT NULL,
  `FDissection` tinyint(1) DEFAULT NULL,
  `FDissectionDesc` varchar(500) DEFAULT NULL,
  `FDissectionAge` int(11) DEFAULT NULL,
  `FFainting` tinyint(1) DEFAULT NULL,
  `FVascularSurgery` varchar(500) DEFAULT NULL,
  `FVascularSurgeryAge` int(11) DEFAULT NULL,
  `FCornealArcus` tinyint(1) DEFAULT NULL,
  `FCornealArcusDesc` varchar(500) DEFAULT NULL,
  `FDiabetes` tinyint(1) DEFAULT NULL,
  `GeneticForHeredityHeart` tinyint(1) DEFAULT NULL,
  `GeneticForHeredityHeartWho` varchar(100) DEFAULT NULL,
  `GeneticForHeredityHeartMutation` tinyint(1) DEFAULT NULL,
  `GeneticForHeredityHeartDoctor` varchar(100) DEFAULT NULL,
  `AnythingElse` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `health_history`
--

INSERT INTO `health_history` (`Id`, `UserId`, `Month`, `Day`, `Year`, `Weight`, `BloodPressure1`, `BloodPressure2`, `Ciggerate`, `Alcohol`, `Diabetes`, `MedicationBP`, `Prescriptions`, `Medication`, `PrescriptionMedication`, `LiverTransplant`, `SeeingDoctor`, `KidneyDetails`, `Cancer`, `CancerDetails`, `OtherCancer`, `ColonPolyps`, `ColonPolypsNumbers`, `MarrowTransplant`, `TransplantedBone`, `TransfusionWeek`, `GeneticTest`, `WhoGenetic`, `Mutation`, `MutationGene`, `Aneurysm`, `AneurysmAge`, `AneurysmLocation`, `Dissection`, `DissectionAge`, `DissectionLocation`, `Fainting`, `Arrythmia`, `ArrythmiaAge`, `ArrythmiaType`, `Cardiomegaly`, `CardiomegalyAge`, `HeartAttack`, `HeartAttackAge`, `CardiacArrest`, `CardiacArrestAge`, `Stroke`, `StrokeAge`, `Cholestrol`, `CholestrolDeposit`, `DepositDetails`, `CornealArcus`, `CornalArcusAge`, `GeneticHeredity`, `GeneticHeredityWho`, `GeneticHeredityMutation`, `Adopted`, `HaveChildren`, `NumbersOfDaughetrs`, `DaughterFName`, `DaughterLName`, `DaughterAge`, `DaughterAlive`, `NumbersOfSons`, `SonFName`, `SonLName`, `SonAge`, `SonAlive`, `Siblings`, `Sisters`, `SisterFName`, `SisterLName`, `SisterAge`, `SisterAlive`, `Brothers`, `BrotherFName`, `BrotherLName`, `BrotherAge`, `BrotherAlive`, `MotherFName`, `MotherLName`, `MotherAge`, `MotherAlive`, `FatherFName`, `FatherLName`, `FatherAge`, `FatherAlive`, `MaternalGrandMotherFName`, `MaternalGrandMotherLName`, `MaternalGrandMotherAge`, `MaternalGrandMotherAlive`, `MaternalGrandFatherFName`, `MaternalGrandFatherLName`, `MaternalGrandFatherAge`, `MaternalGrandFatherAlive`, `ParentalGrandMotherFName`, `ParentalGrandMotherLName`, `ParentalGrandMotherAge`, `ParentalGrandMotherAlive`, `ParentalGrandFatherFName`, `ParentalGrandFatherLName`, `ParentalGrandFatherAge`, `ParentalGrandFatherAlive`, `Country`, `MotherSibling`, `NMS`, `NMB`, `FatherSibling`, `NFS`, `NFB`, `FamilyCancer`, `FBrothersNumberCancer`, `FCancerNames`, `FGeneticForCancerRisk`, `FH`, `FMutationIdentified`, `FMutationGene`, `FamilyMember`, `desc1`, `value1`, `FCardiomyopathy`, `FCardiomyopathyDesc`, `FHeredityArrhthmia`, `FArteriopathy`, `FHeartFailure`, `FHeartFailureDesc`, `FHeartFailureAge`, `FEnlargedHeart`, `FEnlargedHeartDesc`, `FHeartAttack`, `FStroke`, `FStrokeDesc`, `FStrokeAge`, `FAneurysm`, `FDissection`, `FDissectionDesc`, `FDissectionAge`, `FFainting`, `FVascularSurgery`, `FVascularSurgeryAge`, `FCornealArcus`, `FCornealArcusDesc`, `FDiabetes`, `GeneticForHeredityHeart`, `GeneticForHeredityHeartWho`, `GeneticForHeredityHeartMutation`, `GeneticForHeredityHeartDoctor`, `AnythingElse`) VALUES
(44, 'truGene_Ayushi44', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 'temp2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', 'India', NULL, 1, 1, NULL, 2, 2, NULL, 0, NULL, NULL, '', NULL, 'Select Gene', 'Select family member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, 'Select Family doctor', ''),
(45, 'truGene_Ayushi44', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 'temp2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', 'India', NULL, 1, 1, NULL, 2, 2, NULL, 0, NULL, NULL, '', NULL, 'Select Gene', 'Select family member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, 'Select Family doctor', ''),
(46, 'truGene_Ayushi44', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 'temp2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', 'India', NULL, 1, 1, NULL, 2, 2, NULL, 0, NULL, NULL, '', NULL, 'Select Gene', 'Select family member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, 'Select Family doctor', ''),
(47, 'truGene_Ayushi44', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 'temp2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', 'India', NULL, 1, 1, NULL, 2, 2, NULL, 0, NULL, NULL, '', NULL, 'Select Gene', 'Select family member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, 'Select Family doctor', ''),
(48, 'truGene_Ayushi44', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 'temp2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', 'India', NULL, 1, 1, NULL, 2, 2, NULL, 0, NULL, NULL, '', NULL, 'Select Gene', 'Select family member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, 'Select Family doctor', ''),
(49, 'truGene_Ayushi44', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 'temp2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', NULL, 1, '', '', 0, 'Now', 1, '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', '', '', 0, 'Now', 'India', NULL, 1, 1, NULL, 2, 2, NULL, 0, NULL, NULL, '', NULL, 'Select Gene', 'Select family member', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, 'Select Family doctor', '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Phone` varchar(15) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  `Created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Deleted_at` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`Id`, `Name`, `Email`, `Phone`, `Message`, `Created_at`, `Updated_at`, `Deleted_at`) VALUES
(1, '', 'anshikaagg@gmail.com', '89894654', 'Hey there', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, '', 'ayushi@gmail', '88754642', 'Hello', '2019-05-21 18:30:00', '0000-00-00 00:00:00', 0),
(4, '', 'ayushi.intellirise21@gmail.com', '2147483647', 'Inserted', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(5, '', 'abc@gmail.com', '2147483647', 'Timestamp Test', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(6, '', 'abc@gmail.com', '65465', 'Test2', '2019-05-15 20:01:40', '2019-05-15 20:01:40', 1),
(7, '', 'ok@gmail.com', '8968757', ',njh', '2019-05-18 19:40:45', '2019-05-18 19:40:45', 1),
(8, '', 'ok@gmail.com', '8968757', ',njh', '2019-05-18 19:42:50', '2019-05-18 19:42:50', 1),
(9, 'Caroline', 'caroline@gmail.com', '2147483647', 'Need Help', '2019-05-21 02:41:43', '2019-05-21 02:41:43', 1),
(10, 'Handlebars', 'ayushi.intellirise21@gmail.com', '8976546365', 'Handlebars Test', '2019-05-23 05:43:10', '2019-05-23 05:43:10', 1),
(11, 'ayushi', 'ok@gmail.com', '8968757', ',njh', '2019-06-03 05:41:52', '2019-06-03 05:41:52', 1),
(12, 'Rishabh', 'rishabhkumar@gmail.com', '8811223368', 'Your message here!', '2019-06-03 21:47:38', '2019-06-03 21:47:38', 1),
(13, 'ayushi', 'ok@gmail.com', '8968757', ',njh', '2019-06-08 19:45:13', '2019-06-08 19:45:13', 1),
(14, 'ayushi', 'ok@gmail.com', '8968757', ',njh', '2019-06-08 19:54:56', '2019-06-08 19:54:56', 1),
(15, 'ayushi', 'ok@gmail.com', '8968757', ',njh', '2019-06-08 19:56:07', '2019-06-08 19:56:07', 1),
(16, 'ayushi', 'ok@gmail.com', '8968757', ',njh', '2019-06-08 19:56:39', '2019-06-08 19:56:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patientreport`
--

CREATE TABLE `patientreport` (
  `Id` int(11) NOT NULL,
  `patientName` varchar(255) NOT NULL,
  `mobile` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `doctorFirstName` varchar(50) DEFAULT NULL,
  `doctorLastName` varchar(50) DEFAULT NULL,
  `clinicName` varchar(255) DEFAULT NULL,
  `hospitalName` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `upload_report` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patientreport`
--

INSERT INTO `patientreport` (`Id`, `patientName`, `mobile`, `email`, `doctorFirstName`, `doctorLastName`, `clinicName`, `hospitalName`, `city`, `state`, `upload_report`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Emily', 67876654, 'emily@org', 'Mr. Naresh', 'Chadda', 'Sharda', 'City Hospital', 'Noida', 'Uttar Pradesh', 'uploads\\CD_Unit5_Notes-2.pdf', '2019-05-16 21:08:44', '2019-05-16 21:08:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `referpatient`
--

CREATE TABLE `referpatient` (
  `Id` int(11) NOT NULL,
  `PhysicianName` varchar(50) DEFAULT NULL,
  `OfficeContact` varchar(15) DEFAULT NULL,
  `OfficeEmail` varchar(50) DEFAULT NULL,
  `DoctorID` varchar(50) DEFAULT NULL,
  `Mobile` varchar(15) DEFAULT NULL,
  `Message` varchar(255) DEFAULT NULL,
  `PatientName` varchar(50) DEFAULT NULL,
  `PatientDOB` varchar(50) DEFAULT NULL,
  `PatientMobile` varchar(15) DEFAULT NULL,
  `PatientEmail` varchar(50) DEFAULT NULL,
  `PermissionToContact` bit(1) NOT NULL DEFAULT b'0',
  `PatientAddress` varchar(255) DEFAULT NULL,
  `PatientAddress2` varchar(255) DEFAULT NULL,
  `CityState` varchar(50) DEFAULT NULL,
  `Pincode` varchar(10) DEFAULT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DeletedAt` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referpatient`
--

INSERT INTO `referpatient` (`Id`, `PhysicianName`, `OfficeContact`, `OfficeEmail`, `DoctorID`, `Mobile`, `Message`, `PatientName`, `PatientDOB`, `PatientMobile`, `PatientEmail`, `PermissionToContact`, `PatientAddress`, `PatientAddress2`, `CityState`, `Pincode`, `CreatedAt`, `UpdatedAt`, `DeletedAt`) VALUES
(1, 'Anjali Bharghava', '8986542895', 'AnjaliBharghava@org.com', 'Anjali789', '8826457890', 'For Cancer', 'Juhi Gupta', '01031998', '8532032032', 'juhigupta@gmail.com', b'1', 'Marathalli', '', 'Bangalore/ Karnataka', '560066', '2019-06-12 19:25:54', '2019-06-12 19:25:54', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `registrationdetails`
--

CREATE TABLE `registrationdetails` (
  `Id` int(11) NOT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `Email` varchar(100) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `transactionValue` tinyint(4) NOT NULL,
  `Created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Deleted_at` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrationdetails`
--

INSERT INTO `registrationdetails` (`Id`, `FirstName`, `LastName`, `Email`, `Password`, `transactionValue`, `Created_at`, `Updated_at`, `Deleted_at`) VALUES
(1, 'Chris', 'Davis', '@gmail.com', 'laHq7VSQyj', 0, '2019-05-15 20:23:13', '2019-05-15 20:23:13', 1),
(2, 'Joe', 'Nortan', 'Joe.Nortan@gmail.com', '5QXbTK9wql', 0, '2019-05-16 04:32:56', '2019-05-16 04:32:56', 1),
(3, 'fill', 'test', 'ok@gmail.com', 'mnbhbh', 0, '2019-05-18 19:45:25', '2019-05-18 19:45:25', 1),
(4, 'fill', 'test', 'nicedebugging@gmail.com', 'mnbhbh', 0, '2019-05-18 19:46:12', '2019-05-18 19:46:12', 1),
(5, 'John', 'Do', 'johndo@outlook', 'JohnDo', 1, '2019-05-21 03:17:48', '2019-05-21 03:17:48', 1),
(6, 'Rishabh', 'Kumar', 'rishabhkumar@gmail.com', 'rishabh', 0, '2019-06-03 21:48:33', '2019-06-03 21:48:33', 1),
(12, 'Khushi', 'V', 'v.khushi99@gmail.com', 'khushi', 0, '2019-06-03 22:16:10', '2019-06-03 22:16:10', 1),
(14, 'Khushi', 'V', 'v.khushi989@gmail.com', 'khushi', 0, '2019-06-03 22:17:19', '2019-06-03 22:17:19', 1),
(15, 'ravi', 'thakur', 'Ravi@gmail.com', '12345', 0, '2019-06-04 19:58:32', '2019-06-04 19:58:32', 1),
(17, 'ravi', 'thakur', 'Raviiii@gmail.com', '12345', 0, '2019-06-04 20:03:19', '2019-06-04 20:03:19', 1),
(18, 'ravi', 'thakur', 'Raviii@gmail.com', '12345', 0, '2019-06-04 20:10:10', '2019-06-04 20:10:10', 1),
(20, 'ravi', 'thakur', 'Ravii@gmail.com', '12345', 0, '2019-06-04 20:12:20', '2019-06-04 20:12:20', 1),
(21, 'ravi', 'thakur', 'Ravii56@gmail.com', '12345', 0, '2019-06-04 20:12:48', '2019-06-04 20:12:48', 1),
(22, 'ravi', 'thakur', 'Ravii567@gmail.com', '12345', 0, '2019-06-04 20:25:40', '2019-06-04 20:25:40', 1),
(24, 'ravi', 'thakur', 'Ravii5657@gmail.com', '12345', 0, '2019-06-04 20:29:03', '2019-06-04 20:29:03', 1),
(25, 'ayushi', 'varshney', 'mbjj@gc', '13243546', 0, '2019-06-04 20:32:13', '2019-06-04 20:32:13', 1),
(26, 'tru', 'gene', 'trugene@gmail.com', 'trugene', 0, '2019-06-05 04:34:50', '2019-06-05 04:34:50', 1),
(28, 'tru', 'gene', 'trugene1@gmail.com', 'trugene', 0, '2019-06-05 04:36:25', '2019-06-05 04:36:25', 1),
(29, 'ishu', 'jain', 'ishujain@gmail.com', 'ishujain', 0, '2019-06-05 04:41:20', '2019-06-05 04:41:20', 1),
(30, 'ishu', 'jain', 'ishujain1@gmail.com', 'ishujain', 0, '2019-06-05 05:10:23', '2019-06-05 05:10:23', 1),
(31, 'ishu', 'jain', 'ishujain2@gmail.com', 'ishujain', 0, '2019-06-05 05:12:47', '2019-06-05 05:12:47', 1),
(32, 'ishu', 'jain', 'ishujain3@gmail.com', 'ishujain', 0, '2019-06-05 05:13:47', '2019-06-05 05:13:47', 1),
(34, 'ishu', 'jain', 'ishujain4@gmail.com', 'ishujain', 0, '2019-06-05 05:25:10', '2019-06-05 05:25:10', 1),
(44, 'Ayushi', 'Varshney', 'ayushivarshney41@gmail.com', 'ayushi', 0, '2019-06-13 19:37:00', '2019-06-13 19:37:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transactiondetails`
--

CREATE TABLE `transactiondetails` (
  `Id` int(11) NOT NULL,
  `cardNumber` varchar(50) DEFAULT NULL,
  `expiry` varchar(10) DEFAULT NULL,
  `cvv` varchar(5) DEFAULT NULL,
  `promoUsed` varchar(20) DEFAULT NULL,
  `howYouHear` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `Id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(50) DEFAULT NULL,
  `doc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appoinments`
--
ALTER TABLE `appoinments`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `geneservices`
--
ALTER TABLE `geneservices`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `healthhistory`
--
ALTER TABLE `healthhistory`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `health_history`
--
ALTER TABLE `health_history`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `patientreport`
--
ALTER TABLE `patientreport`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `referpatient`
--
ALTER TABLE `referpatient`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `registrationdetails`
--
ALTER TABLE `registrationdetails`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indexes for table `transactiondetails`
--
ALTER TABLE `transactiondetails`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appoinments`
--
ALTER TABLE `appoinments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `geneservices`
--
ALTER TABLE `geneservices`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `healthhistory`
--
ALTER TABLE `healthhistory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `health_history`
--
ALTER TABLE `health_history`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `patientreport`
--
ALTER TABLE `patientreport`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referpatient`
--
ALTER TABLE `referpatient`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `registrationdetails`
--
ALTER TABLE `registrationdetails`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `transactiondetails`
--
ALTER TABLE `transactiondetails`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
