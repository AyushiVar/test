const express = require('express');
const app = express();
const connection = require('./db');
const transportar = require('./mailerAuth');
const path = require('path');
// morgan = require('morgan');
const bodyParser = require('body-parser');
const multer = require('multer');
const sessions = require('express-session');
const hbs = require('express-handlebars');
const crypto = require('crypto');
var PDFDocument = require('pdfkit');
const fs = require('fs');
const generator = require('generate-password');

var session;

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/');
    },
    filename: function(req, file, cb){
        cb(null, file.originalname);
    }
});

const upload = multer({storage: storage});

app.engine('hbs', hbs({extname: 'hbs', defaultLayout: 'layout', layoutsDir: __dirname + '/views/layouts/'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

//app.use(morgan ('short'));
app.use(bodyParser.json({limit:'50mb'})); 
app.use(bodyParser.urlencoded({extended:true, limit:'50mb'}));
app.use(express.static('./views'));

app.use('/uploads', express.static('uploads'));

app.use(sessions({
    secret: '%$#%$65468sdvhv657^&%ryfJS',
    saveUninitialized: true,
    resave: false,
   
}));

app.get('/', function(req, res, next){
    console.log("Responding in Browser..");
    res.sendFile('index.html');
});

app.get('/createPDF', (req, res)=>{
    const doc = new PDFDocument;
    doc.pipe(fs.createWriteStream(__dirname+'/output.pdf'));
    doc.fontSize(25)
       .text('Some text with an embedded font!', 100, 100);
    doc.image(__dirname+'/uploads/flowers.jpg', {
       fit: [250, 300],
       align: 'center',
       valign: 'center'
    });
    doc.end();
    console.log("created");
    res.end("Created");
});

app.get('/gt-file', function (req, res) {
    var filePath = __dirname+"/output.pdf";

    fs.readFile(filePath , function (err,data){
        res.contentType("application/pdf");
        res.send(data);
    });
});

app.get('/downloadFile', function (req, res) {
    var file = __dirname +'/output.pdf';
    res.download(file, function (err) {
        if (err) {
            console.log("Error");
            console.log(err);
        } else {
            console.log("Success");
        }
    });
 });

app.post('/message', (req, res) => {
    console.log(".......");
    const name = req.body.name;
    const email = req.body.email;
    const phone = req.body.phone;
    const message = req.body.message;
    const deleted = 1;

    const querystring = "INSERT INTO Messages (name, email, phone, message, created_at, updated_at, deleted_at)"
                        + "VALUES(?,?,?,?,now(),now(),?)"; 

    connection.query(querystring, [name, email, phone, message, deleted], (err, results, fields)=>{
        if(err){
            return res.status(400).send({ 
                message : error
            });
        }else{
            let mailOptions = {
                from: 'ayushi.intellirise21@gmail.com',
                to: 'ayushivarshney41@gmail.com ',
                subject:'Testing in MySql',
                text: 'Your message has been sent'
            };
            transportar.sendMail(mailOptions, function(error, info){
                if(error){
                    return res.status(400).send({ 
                        message : error
                    });
                }
                else{
                    return res.status(201).send({
                        message: "Your message has been sent "+
                                 "Please check your mail for more info"
                    });
                }
            });
        }
    });
});

app.post('/register', (req, res) =>{
    const fName = req.body.fName;
    const lName = req.body.lName;
    const email = req.body.email;
    const password = (req.body.password).trim();
    const deleted = 1;
    console.log(email);
    if(email === undefined || fName === undefined || lName === undefined || password===undefined ){
        return res.render('create_registration',{registerAlert : 1});
    }
    else{
        console.log("heloo");
        const querystring = "INSERT INTO registrationdetails (firstname, lastname, email, password, transactionValue," 
                            + "created_at, updated_at, deleted_at) VALUES(?,?,?,?,0,now(),now(),?)";

        connection.query(querystring, [fName, lName, email, password, deleted], (err, rows,fields) => {
            if(err){
                return res.render('create_registration',{registerAlert : 2});
            }
            else{
                console.log("1");
                let mailOptions = {
                    from: 'ayushi.intellirise21@gmail.com',
                    to: email,
                    subject:'Sending mail using Node.js',
                    text: 'Hi ' + fName + '\nThank you for registering with us.\n' + 'Please find your email and password below:\n'
                        +'Email: ' + email + '\nPassword: ' +password +'\n'
                          + '\nRegards\nTrugene'
                };
                console.log("2");
                transportar.sendMail(mailOptions, function(error, info){
                    if(error){
                            return res.render('create_registration',{registerAlert : 3});
                    }
                    else{
                        console.log(rows);
                        return res.render('signIn');
                    }
                });
            }
        });
    }
});

app.post('/schedule', (req, res) =>{
    const name = req.body.name;
    const reason = req.body.reason;
    const email= req.body.email;
    const refer = req.body.refer;
    const phone = req.body.phone;
    const message = req.body.message;

    const querystring ="INSERT INTO Appoinments (name, reason, emailid, refferedby, phone, message, created_at," +
                        "updated_at, deleted_at) VALUES(?,?,?,?,?,?,now(),now(),1)";
    connection.query(querystring, [name, reason, email, refer, phone, message], (err, results, fields)=>{
        if(err){
            return res.status(400).send({ 
                message : error
            });
        }else{
        let mailOptions = {
            from: 'ayushi.intellirise21@gmail.com',
            to: 'ayushivarshney41@gmail.com',
            subject:'Sending mail using Node.js',
            text: 'Thank youfor registering with us'
        };
        transportar.sendMail(mailOptions, function(error, info){
            if(error){
                connection.rollback(function(err){
                    console.log(error);
                    return res.status(400).send({ 
                        message : "Invalid Email"
                    });
                }); 
            }
            else{
                res.sendFile('index.html');
            }
        });
    }
    });
});

 app.post('/sendReport',upload.single('report'), (req, res)=>{
    const pat_name = req.body.name;
    const mobile = req.body.mobile;
    const email = req.body.email;
    const doc_fname = req.body.fname;
    const doc_lanme= req.body.lname;
    const clinicName = req.body.clinic;
    const hosName = req.body.hospital;
    const city = req.body.city;
    const state = req.body.state;
    const report = req.file.path;
    
    const querystring = "INSERT INTO patientreport (patientname, mobile, email, doctorfirstname, doctorlastname,"
                        +"clinicname, hospitalname, city, state, upload_report, created_at, updated_at, deleted_at)"
                        +" VALUES(?,?,?,?,?,?,?,?,?,?,now(),now(),1)";
    
    connection.query(querystring, [pat_name,mobile,email,doc_fname,doc_lanme,clinicName,hosName
                        ,city,state,report], (err, rows, fields)=>{
        if(err){
            return res.status(400).send({ 
                message : error
            });
        }
        res.send(Success);
    });
}); 

app.all('/login',(req, res)=>{
    const email = req.body.email;
    const password = req.body.password;
    const querystring = "SELECT * from registrationdetails where email = ? and password = ?";
    connection.query(querystring, [email, password], (err, rows, fields)=>{
        if(err){
            throw err;
        }
        session = req.session;
        if(rows.length > 0){
            if((email).trim() == rows[0].Email && password == rows[0].Password){
                session.uniqueID = "truGene_" + rows[0].FirstName + rows[0].Id;
                session.transaction = rows[0].transactionValue;
                session.firstName = rows[0].FirstName;
                session.lastName = rows[0].LastName;
                session.email = rows[0].Email;
                //console.log(session);
                //console.log(JSON.stringify(rows));
                if(rows[0].transactionValue == 0)
                    res.render('login', {patientName: rows[0].FirstName, lastName: rows[0].LastName});
                else
                    res.render('your-reports', {patientName: rows[0].FirstName, lastName: rows[0].LastName}); 
            }
            else{
                res.send("Invlid User/ Password");
            } 
        }
        else{
            res.render('signIn',{showAlert: 1});
        }
    });
}); 

app.get('/logout', function(req, res){
    req.session.destroy(function(err){
        if(err){
            return res.status(404).send(err);
        }
        else{
            console.log("jghjkgakdbck");
            res.render('signIn');
        }
    })
});

app.get('/redirects',  function(req, res){
    session = req.session;
    if(session.uniqueID){
        res.render('login.hbs');
    }
    else{
        res.end("Session TImeout! To open new session click <a href = '/logout'>Kill Session</a>");
    }
}); 

app.all('/forgotPassword', function(req,res){
    const email = req.body.email;
    const password = generator.generate({
        length: 10,
        numbers: true
    });
    let querystring = "SELECT * FROM registrationdetails WHERE Email = ?";
    connection.query(querystring, [email], (err, rows, field)=>{
        console.log(rows);
        if(err){
            return res.status(400).send({
                message : err
            });
        }
        else if(rows.length == 0){
            console.log("Coming");
            res.render('ForgotPassword',{forgotAlert : 1});
        }
        else{
            querystring = "UPDATE registrationdetails SET Password = ? where Id = ?";
            connection.query(querystring, [password, rows[0].Id], (err, results, fields) =>{
                if(err){
                    connection.rollback(function(){
                        res.render('ForgotPassword',{forgotAlert : 2});
                    });
                }
                else{
                    let mailOptions = {
                        from: 'ayushi.intellirise21@gmail.com',
                        to: email,
                        subject:'Sending mail using Node.js',
                        text: 'Hello ' + rows[0].fname + '\nYour password is:'+password + '\nYou can reset this in your Profile settings'
                        +'\nRegards\nTrugene'
                    };
                    transportar.sendMail(mailOptions, function(error, info){
                        if(error){
                            res.render('ForgotPassword',{forgotAlert : 3}); 
                        }
                        else{
                            res.render('signIn');
                        }
                    });
                }
            });
        }
    });
    
});

app.get('/getBookSession', (req, res)=>{
    session = req.session;
    console.log("Inside getBookSession");
    const querystring = "select ServiceOrder, ServiceDescription from geneservices";
    connection.query(querystring, (err, results, field)=>{
        //console.log(results);
        if(err){
            return res.status(404).send({
                message : "Database Error"
            });
        }
        else{
            res.render('Book-session', {
                data1 : results[0],
                data2 : results[1],
                data3 : results[2],
                sessionTransaction : session.transaction,
                firstName : session.firstName,
                lastName : session.lastName
            });
        } 
        console.log(session.transaction);
    });
});

app.post('/referPatient', (req, res)=>{
    const PhysicianName = req.body.PhysicianName;
    const OfficeContact = req.body.OfficeContact;
    const OfficeEmail = req.body.OfficeEmail;
    const DoctorID = req.body.DoctorID;
    const Mobile = req.body.Mobile;
    const Message = req.body.Message;
    const patientName = req.body.patientName;
    const PatientDOB= req.body.PatientDOB;
    const PatientMobile = req.body.PatientMobile;
    const PatientEmail = req.body.PatientEmail;
    const PermissionToContact = req.body.PermissionToContact;
    const PatientAddress = req.body.PatientAddress;
    const PatientAddress2 = req.body.PatientAddress2;
    const CityState= req.body.CityState;
    const Pincode = req.body.Pincode;

    const querystring = "INSERT INTO referpatient (PhysicianName,OfficeContact, OfficeEmail,DoctorID,Mobile,Message,"+
        "patientName,PatientDOB,PatientMobile,PatientEmail,PermissionToContact,PatientAddress,PatientAddress2,CityState,"+
        "Pincode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    connection.query(querystring, [PhysicianName,OfficeContact, OfficeEmail,DoctorID,Mobile,Message,patientName,PatientDOB,
        PatientMobile,PatientEmail,PermissionToContact,PatientAddress,PatientAddress2,CityState,Pincode], 
        (err, results, fields)=>{

            if(err){
                return res.status(400).send({message:err});
            }else{
                res.json(results);
            }
    });
});

app.post('/healthHistory',upload.single('Prescriptions'), function(req, res){
    const session = req.session;

    console.log(req.file);
    const UserId = session.uniqueID;
    const Month = req.body.Month;
    const Day = req.body.Day;
    const Year = req.body.Year;
    //console.log(Month);
    //const Height = req.body.Height
    const Weight = req.body.Weight;
    //console.log(Weight);
    const BloodPressure1 = req.body.BloodPressure1;
    const BloodPressure2 = req.body.BloodPressure2;
    const Ciggerate = (req.body.Ciggerate === undefined) ? req.body.Ciggerate : (req.body.Ciggerate).trim();
    //console.log("Ciggerte:",Ciggerate);
    const Alcohol = req.body.Alcohol;
    const Diabetes = req.body.Diabetes;
    //console.log(Diabetes);
    const MedicationBP= req.body.MedicationBP;
    const Prescriptions = req.file.path;
    console.log(Prescriptions);
    const Medication = req.body.Medication;
    const PrescriptionMedication = req.body.PrescriptionMedication;
    const LiverTransplant = req.body.LiverTransplant;
    const SeeingDoctor = req.body.SeeingDoctor;
    const KidneyDetails = JSON.stringify(req.body.KidneyDetails);
    console.log(KidneyDetails);
    const Cancer = req.body.Cancer;
    const CancerDetails = JSON.stringify(req.body.CancerDetails);
    const OtherCancer = req.body.OtherCancer;
    const ColonPolyps = req.body.ColonPolyps;
    const ColonPolypsNumbers = req.body.ColonPolypsNumbers;
    const MarrowTransplant = JSON.stringify(req.body.MarrowTransplant);
    const TransplantedBone = JSON.stringify(req.body.TransplantedBone);
    const TransfusionWeek = req.body.TransfusionWeek;
    const GeneticTest = req.body.GeneticTest;
    const WhoGenetic = req.body.WhoGenetic;
    const Mutation = req.body.Mutation;
    const MutationGene = req.body.MutationGene;
    const Aneurysm = req.body.Aneurysm;
    const AneurysmAge = req.body.AneurysmAge;
    const AneurysmLocation = JSON.stringify(req.body.AneurysmLocation);
    const Dissection = req.body.Dissection;
    const DissectionAge = req.body.DissectionAge;
    const DissectionLocation = JSON.stringify(req.body.DissectionLocation);
    const Fainting = req.body.Fainting;
    const Arrythmia = req.body.Arrythmia;
    const ArrythmiaAge = req.body.ArrythmiaAge;
    const ArrythmiaType = JSON.stringify(req.body.ArrythmiaType);
    const Cardiomegaly = req.body.Cardiomegaly;
    const CardiomegalyAge = req.body.CardiomegalyAge;
    const HeartAttack = req.body.HeartAttack;
    const HeartAttackAge = req.body.HeartAttackAge;
    const CardiacArrest = req.body.CardiacArrest;
    const CardiacArrestAge = req.body.CardiacArrestAge;
    const Stroke = req.body.Stroke;
    const StrokeAge = req.body.StrokeAge;
    const Cholestrol = req.body.Cholestrol;
    console.log('f');
    const CholestrolDeposit = req.body.CholestrolDeposit;
    console.log('f');
    const DepositDetails = JSON.stringify(req.body.DepositDetails);
    console.log('f');
    const CornealArcus = req.body.CornealArcus;
    console.log('f');
    const CornalArcusAge = req.body.CornalArcusAge;
    const GeneticHeredity = req.body.GeneticHeredity;
    const GeneticHeredityWho = req.body.GeneticHeredityWho;
    const GeneticHeredityMutation = req.body.GeneticHeredityMutation;

    const Adopted = req.body.Adopted;
    const HaveChildren = req.body.HaveChildren;
    const NumbersOfDaughetrs = req.body.NumbersOfDaughetrs;
    const DaughterFName = req.body.DaughterFName;
    const DaughterLName = req.body.DaughterLName;
    const DaughterAge = req.body.DaughterAge;
    const DaughterAlive = req.body.DaughterAlive;
    const NumbersOfSons = req.body.NumbersOfSons;
    const SonFName = req.body.SonFName;
    const SonLName = req.body.SonLName;
    const SonAge = req.body.SonAge;
    const SonAlive = req.body.SonAlive;

    const Siblings = req.body.Siblings;
    const Sisters = req.body.Sisters;
    const SisterFName = req.body.SisterFName;
    const SisterLName = req.body.SisterLName;
    const SisterAge = req.body.SisterAge ;
    const SisterAlive = req.body.SisterAlive;
    const Brothers = req.body.Brothers;
    const BrotherFName = req.body.BrotherFName;
    const BrotherLName = req.body.BrotherLName;
    const BrotherAge = req.body.BrotherAge;
    const BrotherAlive = req.body.BrotherAlive;
    
    const MotherFName = req.body.MotherFName;
    const MotherLName = req.body.MotherLName;
    const MotherAge = req.body.MotherAge;
    const MotherAlive = req.body.MotherAlive;
    const FatherFName = req.body.FatherFName;
    const FatherLName = req.body.FatherLName;
    const FatherAge = req.body.FatherAge;
    const FatherAlive = req.body.FatherAlive;

    const MaternalGrandMotherFName = req.body.MaternalGrandMotherFName;
    const MaternalGrandMotherLName = req.body.MaternalGrandMotherLName;
    const MaternalGrandMotherAge = req.body.MaternalGrandMotherAge;
    const MaternalGrandMotherAlive = req.body.MaternalGrandMotherAlive;
    const MaternalGrandFatherFName = req.body.MaternalGrandFatherFName;
    const MaternalGrandFatherLName = req.body.MaternalGrandFatherLName;
    const MaternalGrandFatherAge = req.body.MaternalGrandFatherAge;
    const MaternalGrandFatherAlive = req.body.MaternalGrandFatherAlive;

    const ParentalGrandMotherFName = req.body.ParentalGrandMotherFName;
    const ParentalGrandMotherLName = req.body.ParentalGrandMotherLName;
    const ParentalGrandMotherAge= req.body.ParentalGrandMotherAge;
    const ParentalGrandMotherAlive = req.body.ParentalGrandMotherAlive;
    const ParentalGrandFatherFName = req.body.ParentalGrandFatherFName;
    const ParentalGrandFatherLName = req.body.ParentalGrandFatherLName;
    const ParentalGrandFatherAge = req.body.ParentalGrandFatherAge;
    const ParentalGrandFatherAlive = req.body.ParentalGrandFatherAlive;
    console.log('f');

    const Country = req.body.Country;
    const MotherSibling = req.body.MotherSibling;
    const NMS = req.body.NMS;
    const NMB = req.body.NMB;
    console.log('hh');

    const FatherSibling = req.body.FatherSibling;
    const NFS = req.body.NFS;
    const NFB = req.body.NFB;

    const FamilyCancer = JSON.stringify(req.body.FamilyCancer);
    const FBrothersNumberCancer = req.body.FBrothersNumberCancer;
    const FCancerNames = JSON.stringify(req.body.FCancerNames);

    const FGeneticForCancerRisk = req.body.FGeneticForCancerRisk;
    const FH = req.body.FH;
    const FMutationIdentified = req.body.FMutationIdentified;
    const FMutationGene = req.body.FMutationGene;
    const FamilyMember = req.body.FamilyMember;

    console.log('1234');

    const desc1 = req.body.desc1;
    const value1 = JSON.stringify(req.body.value1);
    const FCardiomyopathy = req.body.FCardiomyopathy;
    const FCardiomyopathyDesc = JSON.stringify(req.body.FCardiomyopathyDesc);
    const FHeredityArrhthmia = req.body.FHeredityArrhthmia;
    const FArteriopathy = req.body.FArteriopathy;
    const FHeartFailure = req.body.FHeartFailure;
    const FHeartFailureDesc = JSON.stringify(req.body.FHeartFailureDesc);
    const FHeartFailureAge = req.body.FHeartFailureAge;
    const FEnlargedHeart = req.body.FEnlargedHeart;
    const FEnlargedHeartDesc = JSON.stringify(req.body.FEnlargedHeartDesc);
    const FHeartAttack = req.body.FHeartAttack;
    const FStroke = req.body.FStroke;
    const FStrokeDesc = JSON.stringify(req.body.FStrokeDesc);
    console.log('43');
    const FStrokeAge = req.body.FStrokeAge;
    const FAneurysm = req.body.FAneurysm;
    const FDissection = req.body.FDissection;
    const FDissectionDesc = JSON.stringify(req.body.FDissectionDesc);
    const FDissectionAge = req.body.FDissectionAge;
    const FFainting = req.body.FFainting;
    const FVascularSurgery= JSON.stringify(req.body.FVascularSurgery);
    const FVascularSurgeryAge = req.body.FVascularSurgeryAge;
    const FCornealArcus = req.body.FCornealArcus;
    const FCornealArcusDesc = JSON.stringify(req.body.FCornealArcusDesc);
    const FDiabetes = req.body.FDiabetes;
    const GeneticForHeredityHeart = req.body.GeneticForHeredityHeart;
    const GeneticForHeredityHeartWho = req.body.GeneticForHeredityHeartWho;
    const GeneticForHeredityHeartMutation = req.body.GeneticForHeredityHeartMutation;
    const GeneticForHeredityHeartDoctor = req.body.GeneticForHeredityHeartDoctor;
    const AnythingElse = req.body.AnythingElse;
    
    console.log('f');

    let querystring = "INSERT INTO health_history (UserId, Month, Day, Year, Weight, BloodPressure1, BloodPressure2, Ciggerate, Alcohol, Diabetes,"
        +"MedicationBP,Prescriptions, Medication, PrescriptionMedication,LiverTransplant," 
        +"SeeingDoctor, KidneyDetails, Cancer, CancerDetails, OtherCancer,ColonPolyps,ColonPolypsNumbers,"
        +"MarrowTransplant, TransplantedBone, TransfusionWeek, GeneticTest, WhoGenetic, Mutation,"
        +"MutationGene, Aneurysm, AneurysmAge, AneurysmLocation, Dissection,DissectionAge,"
        +"DissectionLocation,Fainting, Arrythmia, ArrythmiaAge, ArrythmiaType, Cardiomegaly,"
        +"CardiomegalyAge, HeartAttack,HeartAttackAge, CardiacArrest, CardiacArrestAge, Stroke,"
        +"StrokeAge, Cholestrol, CholestrolDeposit,DepositDetails, CornealArcus, CornalArcusAge,"
        +"GeneticHeredity, GeneticHeredityWho, GeneticHeredityMutation,Adopted,HaveChildren,NumbersOfDaughetrs,"
        +"DaughterFName,DaughterLName,DaughterAge, DaughterAlive,NumbersOfSons ,SonFName ,SonLName,SonAge,"
        +"SonAlive ,Siblings,Sisters,SisterFName ,SisterLName ,SisterAge, SisterAlive,Brothers,BrotherFName,"
        +"BrotherLName,BrotherAge,BrotherAlive ,MotherFName,MotherLName ,MotherAge , MotherAlive ,FatherFName ,"
        +"FatherLName,FatherAge ,FatherAlive , MaternalGrandMotherFName ,MaternalGrandMotherLName, MaternalGrandMotherAge," 
        +"MaternalGrandMotherAlive , MaternalGrandFatherFName ,MaternalGrandFatherLName ,MaternalGrandFatherAge,"
        +"MaternalGrandFatherAlive ,ParentalGrandMotherFName ,ParentalGrandMotherLName,ParentalGrandMotherAge,"
        +"ParentalGrandMotherAlive ,ParentalGrandFatherFName ,ParentalGrandFatherLName ,ParentalGrandFatherAge ," 
        +"ParentalGrandFatherAlive ,Country ,MotherSibling ,NMS ,NMB ,FatherSibling , NFS ,NFB ,FamilyCancer,"
        +"FBrothersNumberCancer ,FCancerNames ,FGeneticForCancerRisk ,FH ,FMutationIdentified ,FMutationGene,"
        +"FamilyMember ,desc1 , value1, FCardiomyopathy, FCardiomyopathyDesc ,FHeredityArrhthmia ,FArteriopathy,"
        +"FHeartFailure ,FHeartFailureDesc,FHeartFailureAge ,FEnlargedHeart ,FEnlargedHeartDesc ,FHeartAttack,"
        +"FStroke , FStrokeDesc,FStrokeAge,FAneurysm, FDissection,FDissectionDesc ,FDissectionAge ,FFainting," 
        +"FVascularSurgery, FVascularSurgeryAge ,FCornealArcus ,FCornealArcusDesc ,FDiabetes, GeneticForHeredityHeart,"
        +"GeneticForHeredityHeartWho , GeneticForHeredityHeartMutation, GeneticForHeredityHeartDoctor, AnythingElse)"
                        
                        +"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,?,?,?,?)";    
    connection.query(querystring, [UserId, Month, Day, Year, Weight, BloodPressure1, BloodPressure2, Ciggerate, Alcohol, Diabetes,
                                        MedicationBP,Prescriptions, Medication, PrescriptionMedication,LiverTransplant, 
                                        SeeingDoctor, KidneyDetails, Cancer, CancerDetails, OtherCancer,ColonPolyps,ColonPolypsNumbers,
                                        MarrowTransplant, TransplantedBone, TransfusionWeek, GeneticTest, WhoGenetic, Mutation,
                                        MutationGene, Aneurysm, AneurysmAge, AneurysmLocation, Dissection,DissectionAge,
                                        DissectionLocation,Fainting, Arrythmia, ArrythmiaAge, ArrythmiaType, Cardiomegaly,
                                        CardiomegalyAge, HeartAttack,HeartAttackAge, CardiacArrest, CardiacArrestAge, Stroke,
                                        StrokeAge, Cholestrol, CholestrolDeposit,DepositDetails, CornealArcus, CornalArcusAge,
                                        GeneticHeredity, GeneticHeredityWho, GeneticHeredityMutation,Adopted,HaveChildren,NumbersOfDaughetrs,
                                        DaughterFName,DaughterLName,DaughterAge, DaughterAlive,NumbersOfSons ,SonFName ,SonLName,SonAge,
                                        SonAlive ,Siblings,Sisters,SisterFName ,SisterLName ,SisterAge, SisterAlive,Brothers,BrotherFName,
                                        BrotherLName,BrotherAge,BrotherAlive ,MotherFName,MotherLName ,MotherAge , MotherAlive ,FatherFName ,
                                        FatherLName,FatherAge ,FatherAlive , MaternalGrandMotherFName ,MaternalGrandMotherLName, MaternalGrandMotherAge , 
                                        MaternalGrandMotherAlive , MaternalGrandFatherFName ,MaternalGrandFatherLName ,MaternalGrandFatherAge ,
                                        MaternalGrandFatherAlive ,ParentalGrandMotherFName ,ParentalGrandMotherLName,ParentalGrandMotherAge,
                                        ParentalGrandMotherAlive ,ParentalGrandFatherFName ,ParentalGrandFatherLName ,ParentalGrandFatherAge , 
                                        ParentalGrandFatherAlive ,Country ,MotherSibling ,NMS ,NMB ,FatherSibling , NFS ,NFB ,FamilyCancer ,
                                        FBrothersNumberCancer ,FCancerNames ,FGeneticForCancerRisk ,FH ,FMutationIdentified ,FMutationGene ,
                                        FamilyMember ,desc1 , value1, FCardiomyopathy, FCardiomyopathyDesc ,FHeredityArrhthmia ,FArteriopathy ,
                                        FHeartFailure ,FHeartFailureDesc,FHeartFailureAge ,FEnlargedHeart ,FEnlargedHeartDesc ,FHeartAttack ,
                                        FStroke , FStrokeDesc,FStrokeAge,FAneurysm, FDissection,FDissectionDesc ,FDissectionAge ,FFainting, 
                                        FVascularSurgery, FVascularSurgeryAge ,FCornealArcus ,FCornealArcusDesc ,FDiabetes, GeneticForHeredityHeart,
                                        GeneticForHeredityHeartWho , GeneticForHeredityHeartMutation, GeneticForHeredityHeartDoctor, AnythingElse], 
    function(err,rows,fields){
        if(err){
            console.log("from here");
            return res.send({message:err});
        }else{
            let first='',second='';
            querystring = "SELECT (if((Month = '')|(MONTH is NULL), 1, 0)+if((Day = '')|(DAY is NULL), 1, 0)+if((Year = '')|(Year is NULL), 1, 0)+if(Weight = '', 1, 0)+if(BloodPressure1 = '', 1, 0)+if(BloodPressure2 = '', 1, 0)+if(Prescriptions ='', 1, 0)+if(Medication is NULL, 1, 0)+if(Medication = 1, if(PrescriptionMedication is null, 1, 0), 0)+if(SeeingDoctor is NULL, 1, 0)+if(SeeingDoctor = 1, if(KidneyDetails is null, 1, 0), 0)+if(CANCER is NULL, 1, 0)+if(Cancer = 1, if(CancerDetails is null, 1, if(OtherCancer = '', 1, 0)), 0)+if(ColonPolyps is NULL, 1, 0)+if(ColonPolyps = 1, if(ColonPolypsNumbers = 0, 1, 0), 0)+if(MarrowTransplant = '0', 1, 0)+if(TransplantedBone = '0', 1, 0)+if(GeneticTest is NULL, 1, 0)+if(GeneticTest = 1, if(WhoGenetic = '', 1, 0), 0)+if((GeneticTest = 1)&(MutationGene = ''), 1, 0) +if(Aneurysm is NULL, 1, 0)+if(Aneurysm = 1, if(AneurysmAge = 0, 1, 0), 0)+if(Aneurysm = 1, if(AneurysmLocation is null, 1, 0), 0)+if(Dissection is NULL, 1, 0)+if(Dissection = 1, if(DissectionAge = 0, 1, 0), 0)+if(Dissection = 1, if(DissectionLocation is null, 1, 0), 0)+if(Arrythmia is NULL, 1, 0) +if(Arrythmia = 1, if(ArrythmiaAge = 0, 1, 0), 0)+if(Arrythmia = 1, if(ArrythmiaType is null, 1, 0), 0)+if(Cardiomegaly is NULL, 1, 0) +if(Cardiomegaly = 1, if(CardiomegalyAge = 0, 1, 0), 0) +if(HeartAttack is NULL, 1, 0) +if(HeartAttack = 1, if(HeartAttackAge = 0, 1, 0), 0) +if(CardiacArrest is NULL, 1, 0)+if(CardiacArrest = 1, if(CardiacArrestAge = 0, 1, 0), 0)+if(Stroke is NULL, 1, 0)+if(Stroke = 1, if(StrokeAge = 0, 1, 0), 0)+if(Cholestrol is NULL, 1, 0)+if(CholestrolDeposit is NULL, 1, 0)+if(CholestrolDeposit = 1, if(DepositDetails is null, 1, 0), 0)+if(CornealArcus is NULL, 1, 0)+if((CornealArcus is NULL)&(CornalArcusAge = 0), 1, 0)+if(CornealArcus = 1, if(CornalArcusAge = 0, 1, 0), 0)+if(GeneticHeredity is NULL, 1, 0)+if(GeneticHeredity = 1, if(GeneticHeredityWho is null, 1, 0), 0)) as Null_FirstTab FROM health_history WHERE UserId = ?";
            const querystring1 = "SELECT (if(Adopted is NULL, 1, 0)+if(HaveChildren is NULL, 1, 0)+if(FatherSibling is NULL, 1, 0)+if(MotherSibling is NULL, 1, 0)+if(Country is NULL, 1, 0)+if(Siblings is NULL, 1, 0)+if(DaughterFName = '', 1, 0)+if(DaughterLName = '', 1, 0)+if(DaughterAge = 0, 1, 0)+if(SonFName = '', 1, 0)+if(SonLName = '', 1, 0)+if(SonAge = 0, 1, 0)+if(SisterFName = '', 1, 0)+if(SisterLName = '', 1, 0)+if(SisterAge = 0, 1, 0)+if(BrotherFName = '', 1, 0)+if(BrotherLName = '', 1, 0)+if(BrotherAge = 0, 1, 0)+if(FatherFName = '', 1, 0)+if(FatherLName = '', 1, 0)+if(FatherAge = 0, 1, 0)+if(MotherFName = '', 1, 0)+if(MotherLName = '', 1, 0)+if(MotherAge = 0, 1, 0)+if(MaternalGrandMotherFName = '', 1, 0)+if(MaternalGrandMotherLName = '', 1, 0)+if(MaternalGrandMotherAge = 0, 1, 0)+if(MaternalGrandFatherFName = '', 1, 0)+if(MaternalGrandFatherLName = '', 1, 0)+if(MaternalGrandFatherAge = 0, 1, 0)+if(ParentalGrandMotherFName = '', 1, 0)+if(ParentalGrandMotherLName = '', 1, 0)+if(ParentalGrandMotherAge = 0, 1, 0)+if(ParentalGrandFatherFName = '', 1, 0)+if(ParentalGrandFatherLName = '', 1, 0)+if(ParentalGrandFatherAge = 0, 1, 0)) as Null_SecondTab from health_history where UserId = ?";
            const querystring2 = "SELECT (if(FamilyCancer is NULL, 1, 0)+if(FBrothersNumberCancer = 0, 1, 0)+if(FGeneticForCancerRisk is NULL, 1, 0)+if(FH is NULL, 1, 0)+if(FMutationIdentified is NULL, 1, 0)+if(FMutationGene is NULL, 1, 0)+if(FamilyMember is null, 1, 0)+if(FCardiomyopathy is NULL, 1, 0)+if(FCardiomyopathyDesc is NULL, 1, 0)+if(FHeredityArrhthmia is NULL, 1, 0)+if(FArteriopathy is NULL, 1, 0)+if(FHeartFailure is NULL, 1, 0)+if(FHeartFailureDesc is NULL, 1, 0)+if(FHeartFailureAge = 0, 1, 0)+if(FEnlargedHeart is NULL, 1, 0)+if(FEnlargedHeartDesc is NULL, 1 ,0)+if(FStroke is NULL, 1, 0)+if(FStrokeDesc is NULL, 1, 0)+if(FStrokeAge = 0, 1, 0)+if(FAneurysm is NULL, 1, 0)+if(FDissection is NULL, 1, 0)+if(FDissectionDesc is NULL, 1, 0)+if(FDissectionAge = 0, 1, 0)+if(FFainting is NULL, 1, 0)+if(FVascularSurgery is NULL, 1, 0)+if(FVascularSurgeryAge = 0, 1, 0)+if(FCornealArcus is NULL, 1, 0)+if(FCornealArcusDesc is NULL, 1, 0)+if(FDiabetes is NULL, 1, 0)+if(GeneticForHeredityHeart is NULL, 1, 0)+if(GeneticForHeredityHeartWho is NULL, 1, 0)+if(GeneticForHeredityHeartMutation is NULL, 1, 0)+if(GeneticForHeredityHeartDoctor is NULL, 1, 0)) as Null_SecondTab FROM `health_history` WHERE UserId = ?";
            connection.query(querystring, [UserId], (err, rows, fields)=>{
                if(err){
                    return res.send({message:err});
                }   
                if(rows[0].Null_FirstTab != 0){
                    first = 'Incomplete';
                    //return res.render("Health-History",{historySaved: 1, firstTab: 'Incomplete'});
                }
                else{
                    first = 'Complete';
                    //return res.render("Health-History",{historySaved: 1, firstTab: 'Complete'});
                }
            });
            connection.query(querystring1, [UserId], (err, rows, fields)=>{
                if(err){
                    throw err;
                }
                if(rows[0].Null_SecondTab != 0){
                    second = 'Incomplete';
                    //return res.render("Health-History",{historySaved: 1, firstTab: first, secondTab: second});
                }else{
                    second = 'Created';
                }
            });
            connection.query(querystring2, [UserId], (err, rows, fields)=>{
                if(err){
                    throw err;
                }
                if(rows[0].Null_SecondTab != 0){
                    console.log(rows[0].Null_SecondTab);
                    return res.render("Health-History",{historySaved: 1, firstTab: first, secondTab: second, thirdTab: 'Incomplete'});
                }else{
                    return res.render("Health-History",{historySaved: 1, firstTab: first, secondTab: second, thirdTab: 'Complete'});
                }
            });
        }
    });
});

app.all('/payment/:productName', (req, res, next)=>{
    const session = req.session;
    console.log(req.params);
    /* if(session.email==undefined){
        return res.send("session expired");
    } */
    const querystring = "select * from products;"
    connection.query(querystring,[session.email],(err, rows, fields)=>{
        //console.log(rows);
        if(err){
            res.status(404).send({message:err});
        }else if(req.params.productName == 'Cardiac'){
           
            res.render('payment',{product:rows[0].ProductName, productPrice: rows[0].ProductPrice,
                paymentContinue:0,firstName:session.firstName, lastName: session.lastName, email:session.email});
        }
        else{
            res.render('payment',{product:rows[1].ProductName, productPrice: rows[1].ProductPrice,
                paymentContinue:0,firstName:session.firstName, lastName: session.lastName, email:session.email});
        }
    });
});

app.post('/placed/:product/:productPrice',(req, res, next)=>{
    console.log('Inside Placed');//it will come here after third tab butoon is clicked
    const session = req.session;

    const UserId = session.uniqueID;
    console.log("session id");
    const Quantity = req.body.Quantity;
    const ForWhom = req.body.ForWhom;
    const HaveDoctor= req.body.HaveDoctor;
    const ProductName= req.body.ProductName;
    const ProductPrice = req.params.productPrice;
    const FirstName1 = req.body.FirstName1;
    const LastName1 = req.body.LastName1;
    const RelationShip1 = req.body.RelationShip1;
    const Age1 = req.body.Age1;
    const FirstName2 = req.body.FirstName2;
    const LastName2 = req.body.LastName2;
    const RelationShip2 = req.body.RelationShip2;
    const Age2 = req.body.Age2;
    const FirstName3 = req.body.FirstName3;
    const LastName3 = req.body.LastName3;
    const RelationShip3 = req.body.RelationShip3;
    const Age3 = req.body.Age3;
    const FirstName4 = req.body.FirstName4;
    const LastName4 = req.body.LastName4;
    const RelationShip4 = req.body.RelationShip4;
    const Age4= req.body.Age4;
    const Name = req.body.Name;
    const City = req.body.City;
    const Pin = req.body.Pin;
    const Address1 = req.body.Address1;
    const Phone = req.body.Phone;
    const Address2 = req.body.Address2;
    const Email = req.body.Email;
    const CardNumber = req.body.CardNumber;
    const Valid = req.body.Valid;
    const CVV = req.body.CVV;

    console.log("read all data");
    console.log(Phone);
    let querystring = "INSERT INTO ProductPurchase (UserId,Quantity, ForWhom, HaveDoctor, ProductName, ProductPrice, FirstName1, LastName1, RelationShip1, Age1, FirstName2, LastName2, RelationShip2, Age2, FirstName3, LastName3, RelationShip3, Age3, FirstName4, LastName4, RelationShip4, Age4, Name, City, Pin, Address1, Phone, Address2, Email, CardNumber,Valid, CVV) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    connection.query(querystring,[UserId, Quantity, ForWhom, HaveDoctor, ProductName, ProductPrice, FirstName1, LastName1, RelationShip1, Age1, FirstName2, LastName2, RelationShip2, Age2, FirstName3, LastName3, RelationShip3, Age3, FirstName4, LastName4, RelationShip4, Age4, Name, City, Pin, Address1, Phone, Address2, Email, CardNumber,Valid, CVV],(err, rows, fields)=>{
        console.log("saving..");
        if(err){
            return res.status(404).send({message:err});
        }else{
            querystring = "UPDATE registrationdetails set transactionValue = 0 where Email = ?";
            connection.query(querystring,[session.email],(err,row,field)=>{
                console.log("updating..");
                if(err){
                    return res.status(404).send({message:err});
                }else{
                    console.log(row);
                    
                }
            });
            return res.render('payment',{paymentContinue:1}); 
        }
    });
});

app.all('/signIn', (req, res)=>{
    res.render('signIn');
});
app.all('/your-report',function(req,res,next){
    console.log("Inside your report");
    session = req.session;
    console.log(session);
    if(session.transaction==0){
        res.render('login', {patientName: session.firstName, lastName:session.lastName});
    }
    else{
        res.render('your-reports',{patientName: session.firstName, lastName:session.lastName});
    }
});
app.all('/activation-kit',function(req,res,next){
    session = req.session;
    res.render('Activate-kit',{patientName: session.firstName, lastName: session.lastName});
});
app.all('/registration', (req, res, next)=>{
    res.render('create_registration',{registerAlert: 0});
});
app.all('/forgot',function(req,res,next){
    session = req.session;
    res.render('ForgotPassword',{forgotAlert : 0});
});
app.all('/history', (req, res, next)=>{
    const session = req.session;
    res.render('Health-History',{patientName: session.firstName, lastName:session.lastName});
});
app.all('/product-page',(req, res, next)=>{
    const session = req.session;
    res.render('product-page',{patientName: session.firstName, lastName: session.LastName});
});



if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send('error', {
            message: err.message, 
            error: err
        });
     });
 }

app.listen(3003,function(req, res){
    console.log("The server is up...");
});
